Role Name
=========

Role used to backup tower postgres db. This role is included in the tower installation tar and is added here to be used as a standalone role.

Requirements
------------
ssh access, sudo, postgres user and postgres db name.

Role Variables
--------------

Dependencies
------------


Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
