Role Name
=========
Used to backup tower postgresql db and upload to s3 bucket.

Requirements
------------
ssh access, sudo, postgres user and postgres db name.

Role Variables
--------------
```
my_bucket:
my_object:
aws_access_key:
aws_secret_key:
ec2_url:
``` 
Dependencies
------------


Example Playbook
----------------

    - hosts: all
      roles:
         - 

License
-------

Apache

Author Information
------------------
jlozadad
